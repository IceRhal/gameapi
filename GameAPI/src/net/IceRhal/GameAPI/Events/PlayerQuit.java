package net.IceRhal.GameAPI.Events;

import net.IceRhal.GameAPI.Joueurs.Joueur;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {
	
	@EventHandler
	public void PlayerQuitEvent (PlayerQuitEvent e) {
		
		Player p = e.getPlayer();

		if(!Joueur.contains(p)) return;
		else Joueur.remove(p);
	}

}
