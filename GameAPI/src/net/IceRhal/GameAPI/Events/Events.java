package net.IceRhal.GameAPI.Events;

import java.util.ArrayList;

import net.IceRhal.GameAPI.GameAPI;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class Events {
	
	public static void load() {
				
		ArrayList<Listener> e = new ArrayList<Listener>();
		
		e.add(new PlayerQuit());
		e.add(new EntityDamage());
		e.add(new EntityDamageByEntity());
		
		for(Listener l : e) {
			
			Bukkit.getPluginManager().registerEvents(l, GameAPI.getPlugin());
		}
	}

}
