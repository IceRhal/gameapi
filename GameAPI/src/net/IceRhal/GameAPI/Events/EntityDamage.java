package net.IceRhal.GameAPI.Events;

import net.IceRhal.GameAPI.Joueurs.Joueur;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamage implements Listener {

	@EventHandler
	public void EntityDamageEvent(EntityDamageEvent e) {
		
		Entity ent = e.getEntity();

		if(!Joueur.contains(ent)) return;
		
		Player p = (Player) e.getEntity();
		
		Joueur j = Joueur.getJoueur(p);
		
		if(!j.options.allowDamage()) e.setCancelled(true);
	}
}
