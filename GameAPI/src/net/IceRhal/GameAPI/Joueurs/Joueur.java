package net.IceRhal.GameAPI.Joueurs;

import java.util.HashMap;

import net.IceRhal.GameAPI.Equipes.Equipe;
import net.IceRhal.GameAPI.Rounds.Round;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Joueur {
	
	static HashMap<Player, Joueur> joueurs = new HashMap<Player, Joueur>();
	
	Player p;
	Round round;
	Equipe equipe;
	
	public JoueurOptions options = new JoueurOptions();
	
	public Joueur(Player p, Round round) {		
		
		this.p = p;
		this.round = round;
		
		if(!contains(p)) joueurs.put(p, this);
		else {
			
			remove(p);
			joueurs.put(p, this);
		}
	}
	
	public Player getPlayer() {
		
		return p;
	}
	
	public void setEquipe(Equipe equipe) {
	
		if(this.equipe != null) this.equipe.remove(this); 
			
		this.equipe = equipe;
		
		equipe.add(this);
	}
	
	public Equipe getEquipe() {
		
		return equipe;
	}
	
	public Round getRound() {
		
		return round;
	}
	
	public static boolean contains(Player p) {
		
		return joueurs.containsKey(p);
	}
	
	public static boolean contains(Entity e) {
		
		return joueurs.containsKey(e);
	}

	public static void remove(Player p) {
		
		Joueur j = joueurs.get(p);
		
		j.getEquipe().remove(j);
		j.getRound().remove(j);
		
		joueurs.remove(p);
	}
	
	public static Joueur getJoueur(Player p) {
		
		return joueurs.get(p);
	}
}
