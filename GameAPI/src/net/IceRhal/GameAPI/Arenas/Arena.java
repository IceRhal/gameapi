package net.IceRhal.GameAPI.Arenas;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

import net.IceRhal.GameAPI.Games.Game;

public class Arena {

	static ArrayList<Arena> arenas = new ArrayList<Arena>();
	
	Game game;
	String name;
	Location spawn;
	Boolean isUse;
	Integer minPlayer;
	Integer maxPlayer;	
	
	public ArenaOptions options = new ArenaOptions();
	
	public Arena(Game game, String name, Location spawn, Integer minPlayer, Integer maxPlayer) {

		this.game = game;
		this.name = name;
		this.spawn = spawn;
		
		isUse = false;		
		
		if(minPlayer < 1) minPlayer = 1;
		if(maxPlayer != -1 && maxPlayer < minPlayer) maxPlayer = minPlayer;
		
		this.minPlayer = minPlayer;
		this.maxPlayer = maxPlayer;		
	}
	
	public Arena(Game game, String name, Location spawn) {

		this(game, name, spawn, 1, -1);		
	}
	
	public Location getSpawn() {
		
		return spawn;
	}
		
	public static List<Arena> getArenas() {
		
		return arenas;
	}
	
	public boolean isUse() {
		
		return isUse;		
	}
	
	public int getMin() {
		
		return minPlayer;
	}
	
	public int getMax() {
		
		return maxPlayer;
	}
}
