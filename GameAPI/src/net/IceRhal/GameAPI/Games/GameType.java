package net.IceRhal.GameAPI.Games;

public enum GameType {
	
	PVP,
	
	BUILD,
	
	ADVENTURE,
	
	PARKOUR,
	
	COMPETITION,
	
	OTHER;
}
