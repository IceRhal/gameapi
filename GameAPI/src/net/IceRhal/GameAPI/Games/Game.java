package net.IceRhal.GameAPI.Games;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

import net.IceRhal.GameAPI.Arenas.Arena;
import net.IceRhal.GameAPI.Rounds.Round;

public class Game {

	static ArrayList<Game> games = new ArrayList<Game>();
	
	String name;
	GameType gameType;

	public Game(String name, GameType gameType) {
		
		this.name = name;
		this.gameType = gameType;
		
		games.add(this);
	}	
	
	public List<Game> getList() {
		
		return games;
	}
	
	public Arena createArena(String name, Location spawn) {
		
		return new Arena(this, name, spawn);
	}
	
	public Arena createArena(String name, Location spawn, Integer minPlayer) {
		
		return new Arena(this, name, spawn, minPlayer, -1);
	}
	
	public Arena createArena(String name, Location spawn, Integer minPlayer, Integer maxPlayer) {
		
		return new Arena(this, name, spawn, minPlayer, maxPlayer);
	}
	
	public Round createRound() {
		
		return new Round(this);
	}
}
