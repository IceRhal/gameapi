package net.IceRhal.GameAPI.Rounds;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import net.IceRhal.GameAPI.Arenas.Arena;
import net.IceRhal.GameAPI.Equipes.Equipe;
import net.IceRhal.GameAPI.Games.Game;
import net.IceRhal.GameAPI.Joueurs.Joueur;

public class Round {

	static ArrayList<Round> rounds = new ArrayList<Round>();
	
	Game game;
	Arena arena;
	Boolean play;
	
	Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
	
	public RoundOptions options;
	
	ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
	ArrayList<Equipe> equipes = new ArrayList<Equipe>();
	
	ArrayList<Joueur> inLife = new ArrayList<Joueur>();
	
	public Round(Game game) {

		this.game = game;
		
		options = new RoundOptions();
		
		play = false;
	}
	
	public static List<Round> getList() {
		
		return rounds;
	}
	
	public Joueur addJoueur(Player p) {
		
		Joueur j = new Joueur(p, this);
		
		joueurs.add(j);
		
		p.setScoreboard(scoreboard);
		
		return j;
	}
	
	public void remove(Joueur j) {
		
		if(!joueurs.contains(j)) return;
		
		if(!play) joueurs.remove(j);
		
		else ;//TODO
	}
	
	public List<Joueur> getJoueurs() {
		
		return joueurs;
	}
	
	public List<Joueur> getInLifeJoueurs() {
		
		return inLife;
	}
	
	public Equipe createEquipe(String name) {
		
		Equipe equipe = new Equipe(this, name);
		
		equipes.add(equipe);
		
		return equipe;
	}
	
	public Equipe createRandomEquipe(String name) {
		
		Equipe equipe = new Equipe(this, name);
				
		return equipe;
	}
	
	public Scoreboard getScoreboard() {		
		
		return scoreboard;
	}
	
	public StartError start(Arena a) {
		
		if(a == null) return StartError.ARENA_NULL;
		
		if(a.isUse()) return StartError.ARENA_IS_USE;
		
		if(joueurs.isEmpty()) return StartError.NO_PLAYERS;
		
		if(a.getMin() > joueurs.size()) return StartError.INSUFFICIENT_PLAYERS;
		
		if(a.getMax() != -1 && a.getMax() < joueurs.size()) return StartError.TOO_MUCH_PLAYERS;
			
		play = true;
			
		inLife = joueurs;
			
		for(Joueur j : joueurs) {
				
			j.getPlayer().teleport(a.getSpawn());
		}

		return null;
	}
	
	public enum StartError {
		
		ARENA_NULL,
		
		ARENA_IS_USE,
		
		NO_PLAYERS,
		
		INSUFFICIENT_PLAYERS,
		
		TOO_MUCH_PLAYERS;
	}
}
