package net.IceRhal.GameAPI.Rounds;

import net.IceRhal.GameAPI.Objectifs.Objectif;

public class RoundOptions {

	Objectif obj;	

	Boolean friendlyFire = true;
	Boolean foodDecrease = true;
	Boolean useEquipe = false;	
	
	public void setObjectif(Objectif obj) {
		
		this.obj = obj;
	}
	
	public Objectif getObjectif() {
		
		return obj;
	}
		
	public void setUseEquipe(Boolean flag) {
		
		useEquipe = flag;
	}
	
	public boolean getUseEquipe() {
		
		return useEquipe;
	}
		
	public void setFriendlyFire(Boolean flag) {
		
		friendlyFire = flag;
	}
	
	public boolean getFriendlyFire() {
		
		return friendlyFire;
	}
	
	public void setFoodDecrease(Boolean flag) {
		
		foodDecrease = flag;
	}
	
	public boolean getFoodDecrease() {
		
		return foodDecrease;
	}
}
