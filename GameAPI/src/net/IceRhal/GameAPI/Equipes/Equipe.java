package net.IceRhal.GameAPI.Equipes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.scoreboard.Team;

import net.IceRhal.GameAPI.Joueurs.Joueur;
import net.IceRhal.GameAPI.Rounds.Round;

public class Equipe {
	
	Round round;
	String name;
	EquipeType type;
	Team t;
	
	ArrayList<Joueur> joueurs = new ArrayList<Joueur>();
	
	public EquipeOptions options;
	
	public Equipe(Round round, String name) {
		
		this(round, name, EquipeType.NORMAL);
	}
	
	public Equipe(Round round, String name, EquipeType type) {
		
		this.round = round;
		this.name = name;
		this.type = type;
		
		this.options = new EquipeOptions();
		
		t = round.getScoreboard().registerNewTeam(name);
	}	
	
	public void add(Joueur j) {
		
		joueurs.add(j);
		
		t.addPlayer(j.getPlayer());
	}
	
	public void remove(Joueur j) {
		
		if(!joueurs.contains(j)) return;
		
		joueurs.remove(j);
		
		t.removePlayer(j.getPlayer());
	}
	
	public List<Joueur> getJoueurs() {
		
		return joueurs;
	}
	
	public void updateOptions() {
	
		t.setPrefix(options.getPrefix());
		t.setSuffix(options.getSuffix());
	}
	
	public enum EquipeType {	
		
		NORMAL,
		
		RANDOM;	
	}
}


