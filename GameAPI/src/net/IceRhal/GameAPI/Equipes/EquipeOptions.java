package net.IceRhal.GameAPI.Equipes;

public class EquipeOptions {
	
	Boolean friendlyFire;
	Boolean foodDecrease;
	String prefix;
	String suffix;
		
	public void setFriendlyFire(Boolean flag) {
		
		friendlyFire = flag;
	}
	
	public boolean getFriendlyFire() {
		
		return friendlyFire;
	}
	
	public void setFoodDecrease(Boolean flag) {
		
		foodDecrease = flag;
	}
	
	public boolean getFoodDecrease() {
		
		return foodDecrease;
	}
	
	public void setPrefix(String prefix) {
		
		this.prefix = prefix;
	}
	
	public String getPrefix() {
		
		return prefix;
	}
	
	public void setSuffix(String suffix) {
		
		this.suffix = suffix;
	}
	
	public String getSuffix() {
		
		return suffix;
	}
}
