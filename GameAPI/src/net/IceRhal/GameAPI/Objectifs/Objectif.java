package net.IceRhal.GameAPI.Objectifs;

import net.IceRhal.GameAPI.Rounds.Round;

public abstract class Objectif {

	Round r;
	
	Objectif(Round round) {
		
		r = round;
	}
	
	
	abstract public boolean isCompleted();
}
