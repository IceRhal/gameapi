package net.IceRhal.GameAPI.Objectifs;

import net.IceRhal.GameAPI.Equipes.Equipe;
import net.IceRhal.GameAPI.Joueurs.Joueur;
import net.IceRhal.GameAPI.Rounds.Round;

public class ObjectifLastMan extends Objectif {

	public ObjectifLastMan(Round round) {
		
		super(round);
	}
	
	public boolean isCompleted() {
		
		if(r.getInLifeJoueurs().size() == 1) return true;
		
		if(r.options.getUseEquipe()) {
			
			Equipe e = null;
			
			for(Joueur j : r.getInLifeJoueurs()) {
				
				if(e == null) e = j.getEquipe();
				else if(j.getEquipe() != e) return false;				
			}
			
			return true;
		}	
		else return false;
	}

}
