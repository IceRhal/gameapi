package net.IceRhal.GameAPI.Objectifs;

import net.IceRhal.GameAPI.Rounds.Round;

public class ObjectifCustom extends Objectif {

	Boolean completed;
	
	public ObjectifCustom(Round round) {
		
		super(round);
	}
	
	public boolean isCompleted() {
		
		return completed;
	}
	
	public void setCompleted(Boolean flag) {
		
		completed = flag;
	}

}
