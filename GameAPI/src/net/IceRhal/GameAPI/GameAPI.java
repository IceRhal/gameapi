package net.IceRhal.GameAPI;

import net.IceRhal.GameAPI.Events.Events;

import org.bukkit.plugin.java.JavaPlugin;

public class GameAPI extends JavaPlugin {	
	
	static GameAPI plugin;	
	
	public void onEnable() {				
		
		plugin = this;
		
		Events.load();
	}
	
	public void onDisable() {
		
		
	}	
	
	public static GameAPI getPlugin() {
		
		return plugin;
	}
}
